package com.example.cleanfood

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.example.cleanfood.storage.CleanFoodDatabase
import com.example.cleanfood.storage.Establishment
import com.example.cleanfood.util.Constants
import kotlinx.android.synthetic.main.establishment_item.view.*


class EstablishmentAdapter(private val context: Context) : RecyclerView.Adapter<EstablishmentAdapter.ViewHolder>() {

    companion object {
        private val TAG = EstablishmentAdapter::class.java.simpleName
    }

    private val mHandler = Handler()
    var mEstablishments: MutableList<Establishment> = ArrayList<Establishment>()


    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        val establishmentView = inflater.inflate(R.layout.establishment_item, parent, false)

        return ViewHolder(establishmentView)
    }

    override fun getItemCount(): Int {
        return mEstablishments.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.nameTextView.text = mEstablishments[position].name
        viewHolder.typeTextView.text = mEstablishments[position].businessTypeName
        val distance = mEstablishments[position].distance
        viewHolder.distanceTextView.text = if (distance > 0) {
            "${"%.2f".format(distance)} miles away | ${mEstablishments[position].address}"
        } else {
            mEstablishments[position].address
        }


        if (mEstablishments[position].isFavorite) {
            viewHolder.favoriteImageView.setImageResource(R.drawable.ic_favorite_purple_24dp)
        } else {
            viewHolder.favoriteImageView.setImageResource(R.drawable.ic_favorite_border_purple_24dp)
        }

        val numericRating = mEstablishments[position].ratingValue.toFloatOrNull()
        if (numericRating != null) {
            viewHolder.fhisTextView.visibility = View.GONE

            viewHolder.fhrsRatingBar.rating = numericRating
            viewHolder.fhrsRatingBar.visibility = View.VISIBLE

        } else {
            viewHolder.fhrsRatingBar.visibility = View.GONE

            viewHolder.fhisTextView.text = mEstablishments[position].ratingValue
            viewHolder.fhisTextView.visibility = View.VISIBLE
        }

        viewHolder.favoriteImageView.setOnClickListener {
            mHandler.post(FavoriteHandler(position))
        }
        viewHolder.itemView.setOnClickListener {
            val intent = Intent(context, EstablishmentActivity::class.java).apply {
                putExtra(Constants.ESTABLISHMENT_ID, mEstablishments[position])
            }
            context.startActivity(intent)
        }
    }

    private inner class FavoriteHandler(private val position: Int) : Runnable {
        override fun run() {
            mEstablishments[position].isFavorite = !mEstablishments[position].isFavorite

            if (mEstablishments[position].isFavorite) {
                CleanFoodDatabase.getDatabase(context)
                    .establishmentDao().insert(mEstablishments[position])
                Log.i(TAG, "Added establishment as favorite")
            } else {
                CleanFoodDatabase.getDatabase(context)
                    .establishmentDao().delete(mEstablishments[position])
                Log.i(TAG, "Removed establishment as favorite")
            }

            notifyItemChanged(position)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.search_result_title_text_view
        val typeTextView: TextView = itemView.search_result_type_text_view
        val distanceTextView: TextView = itemView.search_result_distance_text_view
        val favoriteImageView: ImageView = itemView.search_result_favorite_image_view
        val fhrsRatingBar: RatingBar = itemView.fhrs_rating_bar
        val fhisTextView: TextView = itemView.fhis_text_view
    }
}