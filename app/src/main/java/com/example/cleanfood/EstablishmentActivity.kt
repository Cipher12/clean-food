package com.example.cleanfood

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.example.cleanfood.storage.CleanFoodDatabase
import com.example.cleanfood.storage.Establishment
import com.example.cleanfood.util.Constants
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_establishment.*
import java.time.Instant
import java.time.ZoneId

class EstablishmentActivity : AppCompatActivity(),
    OnMapReadyCallback {

    private lateinit var mEstablishment: Establishment
    private lateinit var mDatabase: CleanFoodDatabase

    private var mHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_establishment)

        supportActionBar?.hide()

        mDatabase = CleanFoodDatabase.getDatabase(this)
        mEstablishment = intent.getSerializableExtra(Constants.ESTABLISHMENT_ID) as Establishment

        val mapFragment = supportFragmentManager.findFragmentById(R.id.est_map_view) as SupportMapFragment
        mapFragment.getMapAsync(this)

        est_name_text_view.text = mEstablishment.name

        val ratingValue = mEstablishment.ratingValue
        val date = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Instant.ofEpochSecond(mEstablishment.ratingTimestamp).atZone(ZoneId.systemDefault()).toLocalDate()
        } else {
            null
        }

        val fullRating = if (date == null) {
            "Received rating $ratingValue"
        } else {
            "Received rating $ratingValue on $date"
        }
        est_rating_text_view.text = fullRating

        val fullAddress = "${mEstablishment.address}\n${mEstablishment.postCode}"
        est_address_text_view.text = fullAddress

        val hygieneScore = "Hygiene: ${mEstablishment.hygieneScore}"
        est_hygiene_score_text_view.text = hygieneScore

        val structuralScore = "Structural: ${mEstablishment.structuralScore}"
        est_structural_score_text_view.text = structuralScore

        val confidenceScore = "Confidence in management: ${mEstablishment.confidenceInManagementScore}"
        est_confidence_score_text_view.text = confidenceScore

        if (mEstablishment.isFavorite) {
            est_favorite_image_button.setImageResource(R.drawable.ic_favorite_purple_24dp)
        } else {
            est_favorite_image_button.setImageResource(R.drawable.ic_favorite_border_purple_24dp)
        }

        est_directions_image_button.setOnClickListener {
            val mapsUri =
                Uri.parse("geo:${mEstablishment.latitude},${mEstablishment.longitude}?q=${mEstablishment.name}")
            val mapsIntent = Intent(Intent.ACTION_VIEW, mapsUri)
            mapsIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapsIntent)
        }

        est_favorite_image_button.setOnClickListener {
            mEstablishment.isFavorite = !mEstablishment.isFavorite

            if (mEstablishment.isFavorite) {
                mDatabase.establishmentDao().insert(mEstablishment)
                est_favorite_image_button.setImageResource(R.drawable.ic_favorite_purple_24dp)
            } else {
                mDatabase.establishmentDao().delete(mEstablishment)
                est_favorite_image_button.setImageResource(R.drawable.ic_favorite_border_purple_24dp)
            }
        }

        est_share_image_button.setOnClickListener {
            val shareIntent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_TEXT,
                    "${mEstablishment.name}, ${mEstablishment.address} ${mEstablishment.postCode}"
                )
                type = "text/plain"
            }
            startActivity(shareIntent)
        }

        est_back_image_button.setOnClickListener {
            this.finish()
        }

        mHandler.post {
            val authority = mDatabase.authorityDao().getByIdCode(mEstablishment.localAuthorityCode)
            authority.firstOrNull()?.let {
                val name = "${it.name} local authority"
                est_authority_name_text_view.text = name

                est_authority_email_text_view.text = it.email
                est_authority_website_text_view.text = it.url
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        val latLng = LatLng(mEstablishment.latitude, mEstablishment.longitude)
        googleMap?.let {
            it.addMarker(MarkerOptions().position(latLng))
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.5f))
            it.setMinZoomPreference(12.5f)
            it.setMaxZoomPreference(20.0f)
        }
    }


}
