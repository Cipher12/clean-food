package com.example.cleanfood.search

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.*
import com.example.cleanfood.R
import com.example.cleanfood.storage.CleanFoodDatabase
import com.example.cleanfood.util.Parameters
import java.io.Serializable
import kotlin.math.roundToInt

class FilterDialogFragment : DialogFragment() {

    companion object {
        private const val ANY = "Any"

        private val TAG = FilterDialogFragment::class.java.simpleName
        private val DISTANCE_OPTIONS = arrayOf("1", "2", "3", "5", "8", "13", "21", "unlimited")

        private const val LISTENER_KEY = "listener"
        private val cachedParameters: MutableMap<String, Int> = HashMap()

        fun newInstance(listener: FilterDialogListener): FilterDialogFragment {
            val args = Bundle()
            args.putSerializable(LISTENER_KEY, listener)
            val fragment = FilterDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var mDatabase: CleanFoodDatabase
    private lateinit var mListener: FilterDialogListener
    private lateinit var mSortBySpinner: Spinner
    private lateinit var mAuthoritySpinner: Spinner
    private lateinit var mBusinessTypeSpinner: Spinner
    private lateinit var mRatingOperatorSpinner: Spinner
    private lateinit var mRatingSpinner: Spinner
    private lateinit var mRegionSpinner: Spinner
    private lateinit var mSchemeTypeSpinner: Spinner
    private lateinit var mDistanceNumberPicker: NumberPicker
    private lateinit var mDistanceUnitSpinner: Spinner

    interface FilterDialogListener : Serializable {
        fun onDialogPositiveClick(parameters: MutableMap<String, String>)
        fun onDialogNeutralClick(parameters: MutableMap<String, String>)
        fun onDialogNegativeClick()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mListener = arguments?.getSerializable(LISTENER_KEY) as FilterDialogListener
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            mDatabase = CleanFoodDatabase.getDatabase(it)
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let {
            val builder = AlertDialog.Builder(it)

            val inflater = requireActivity().layoutInflater
            val dialogView = inflater.inflate(R.layout.filter_dialog, null)

            context?.let { context ->
                val mDatabase = CleanFoodDatabase.getDatabase(context)

                builder.setView(dialogView)
                    .setPositiveButton(R.string.search) { _, _ ->
                        mListener.onDialogPositiveClick(generateParameters())
                    }
                    .setNeutralButton(R.string.apply_filters) { _, _ ->
                        mListener.onDialogNeutralClick(generateParameters())
                    }
                    .setNegativeButton(R.string.clear_filters) { _, _ ->
                        cachedParameters.clear()
                        mListener.onDialogNegativeClick()
                    }

                val sortOptionAdapter =
                    ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_spinner_dropdown_item,
                        mDatabase.sortOptionDao().getAllNames()
                    )
                mSortBySpinner = dialogView.findViewById(R.id.sort_by_filter_spinner) as Spinner
                mSortBySpinner.adapter = sortOptionAdapter
                mSortBySpinner.setSelection(cachedParameters[Parameters.SORT_OPTION_KEY] ?: 0)

                val businessTypeAdapter =
                    ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_spinner_dropdown_item
                    )
                businessTypeAdapter.add(ANY)
                businessTypeAdapter.addAll(mDatabase.businessTypeDao().getAllNames())
                mBusinessTypeSpinner = dialogView.findViewById(R.id.business_type_filter_spinner) as Spinner
                mBusinessTypeSpinner.adapter = businessTypeAdapter
                mBusinessTypeSpinner.setSelection(cachedParameters[Parameters.BUSINESS_TYPE_ID] ?: 0)

                val ratingOperatorAdapter =
                    ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_spinner_dropdown_item,
                        mDatabase.ratingOperatorDao().getAllNames()
                    )
                mRatingOperatorSpinner = dialogView.findViewById(R.id.rating_operator_filter_spinner) as Spinner
                mRatingOperatorSpinner.adapter = ratingOperatorAdapter
                mRatingOperatorSpinner.setSelection(cachedParameters[Parameters.RATING_OPERATOR_KEY] ?: 0)

                val regionAdapter =
                    ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_spinner_dropdown_item
                    )
                regionAdapter.add(ANY)
                regionAdapter.addAll(mDatabase.regionDao().getAllNames())
                mRegionSpinner = dialogView.findViewById(R.id.region_filter_spinner) as Spinner
                mRegionSpinner.adapter = regionAdapter
                mRegionSpinner.setSelection(cachedParameters[Parameters.REGION] ?: 0)
                mRegionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        parent?.let {
                            updateLocalAuthorities(getSchemeTypeId(), mRegionSpinner.selectedItem.toString())
                        }
                    }
                }

                val schemeTypeAdapter =
                    ArrayAdapter<String>(
                        context,
                        android.R.layout.simple_spinner_dropdown_item,
                        mDatabase.schemeTypeDao().getAllNames()
                    )
                mSchemeTypeSpinner = dialogView.findViewById(R.id.scheme_type_filter_spinner) as Spinner
                mSchemeTypeSpinner.adapter = schemeTypeAdapter
                mSchemeTypeSpinner.setSelection(cachedParameters[Parameters.SCHEME_TYPE_KEY] ?: 0)

                mSchemeTypeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        parent?.let {
                            val schemeTypeId = getSchemeTypeId()
                            updateRatings(schemeTypeId)
                            updateLocalAuthorities(schemeTypeId, mRegionSpinner.selectedItem.toString())
                        }
                    }
                }

                mAuthoritySpinner = dialogView.findViewById(R.id.authority_filter_spinner) as Spinner
                mRatingSpinner = dialogView.findViewById(R.id.rating_value_filter_spinner) as Spinner
                mDistanceUnitSpinner = dialogView.findViewById(R.id.unit_distance_filter_spinner) as Spinner
                mDistanceUnitSpinner.setSelection(cachedParameters[Parameters.DISTANCE_UNIT] ?: 0)

                mDistanceNumberPicker = dialogView.findViewById(R.id.distance_filter_number_picker) as NumberPicker
                mDistanceNumberPicker.minValue = 0
                mDistanceNumberPicker.maxValue = DISTANCE_OPTIONS.size - 1
                mDistanceNumberPicker.displayedValues = DISTANCE_OPTIONS
                mDistanceNumberPicker.value = cachedParameters[Parameters.MAX_DISTANCE_LIMIT] ?: 2
                mDistanceNumberPicker.wrapSelectorWheel = true
            }

            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onPause() {
        super.onPause()
        this.dismiss()
    }

    private fun getSchemeTypeId(): Int {
        val schemeTypeName = mSchemeTypeSpinner.selectedItem.toString()
        val schemeTypeList = mDatabase.schemeTypeDao().getIdByName(schemeTypeName)
        return if (schemeTypeList.size == 1) {
            schemeTypeList[0]
        } else {
            -1
        }
    }

    private fun updateRatings(schemeTypeId: Int) {
        context?.let { context ->
            val ratingAdapter =
                ArrayAdapter<String>(
                    context,
                    android.R.layout.simple_spinner_dropdown_item,
                    mDatabase.ratingDao().getAllNamesBySchemeType(schemeTypeId)
                )
            mRatingSpinner.adapter = ratingAdapter
            mRatingSpinner.setSelection(cachedParameters[Parameters.RATING_KEY] ?: 0)
        }
    }

    private fun updateLocalAuthorities(schemeTypeId: Int, regionName: String) {
        context?.let { context ->
            val authorityAdapter =
                ArrayAdapter<String>(
                    context,
                    android.R.layout.simple_spinner_dropdown_item
                )
            authorityAdapter.add(ANY)
            val authorities = if (regionName == "Any") {
                Log.i(TAG, "Updating based on scheme type")
                mDatabase.authorityDao().getAllNamesBySchemeType(schemeTypeId)
            } else {
                Log.i(TAG, "Updating based on scheme type and region")
                mDatabase.authorityDao().getAllNamesBySchemeTypeAndRegion(schemeTypeId, regionName)
            }
            if (authorities.isEmpty()) {
                Toast.makeText(
                    context,
                    "There are no valid local authorities with the current configuration. Defaulting to any.",
                    Toast.LENGTH_LONG
                ).show()
            }
            authorityAdapter.addAll(authorities)
            mAuthoritySpinner.adapter = authorityAdapter
            mAuthoritySpinner.setSelection(cachedParameters[Parameters.LOCAL_AUTHORITY_ID] ?: 0)
        }
    }

    private fun convertToMiles(km: String): String {
        return (km.toDouble() / 1.609).roundToInt().toString()
    }

    private fun generateParameters(): MutableMap<String, String> {
        val parameters = HashMap<String, String>()

        cachedParameters.clear()

        cachedParameters[Parameters.SORT_OPTION_KEY] = mSortBySpinner.selectedItemPosition
        cachedParameters[Parameters.LOCAL_AUTHORITY_ID] = mAuthoritySpinner.selectedItemPosition
        cachedParameters[Parameters.BUSINESS_TYPE_ID] = mBusinessTypeSpinner.selectedItemPosition
        cachedParameters[Parameters.RATING_OPERATOR_KEY] = mRatingOperatorSpinner.selectedItemPosition
        cachedParameters[Parameters.RATING_KEY] = mRatingSpinner.selectedItemPosition
        cachedParameters[Parameters.REGION] = mRegionSpinner.selectedItemPosition
        cachedParameters[Parameters.SCHEME_TYPE_KEY] = mSchemeTypeSpinner.selectedItemPosition
        cachedParameters[Parameters.MAX_DISTANCE_LIMIT] = mDistanceNumberPicker.value
        cachedParameters[Parameters.DISTANCE_UNIT] = mDistanceUnitSpinner.selectedItemPosition

        val maxDistance = DISTANCE_OPTIONS[mDistanceNumberPicker.value]
        if (maxDistance != "unlimited") {
            val distanceUnit = mDistanceUnitSpinner.selectedItem.toString()
            when (distanceUnit) {
                "mi" -> parameters[Parameters.MAX_DISTANCE_LIMIT] = maxDistance
                "km" -> parameters[Parameters.MAX_DISTANCE_LIMIT] = convertToMiles(maxDistance)
            }
        } else {
            parameters[Parameters.MAX_DISTANCE_LIMIT] = maxDistance
        }

        val businessTypeName = mBusinessTypeSpinner.selectedItem.toString()
        if (businessTypeName != ANY) {
            val businessTypeId = mDatabase.businessTypeDao().getIdByName(businessTypeName)
            if (businessTypeId.size == 1) {
                parameters[Parameters.BUSINESS_TYPE_ID] = businessTypeId.first().toString()
            }
        }

        val schemeTypeName = mSchemeTypeSpinner.selectedItem.toString()
        val schemeTypeKey = mDatabase.schemeTypeDao().getKeyByName(schemeTypeName)
        if (schemeTypeKey.size == 1) {
            parameters[Parameters.SCHEME_TYPE_KEY] = schemeTypeKey.first()

            if (schemeTypeKey.first() == "FHRS") {
                val ratingOperatorName = mRatingOperatorSpinner.selectedItem.toString()
                val ratingOperatorKey = mDatabase.ratingOperatorDao().getKeyByName(ratingOperatorName)
                if (ratingOperatorKey.size == 1) {
                    parameters[Parameters.RATING_OPERATOR_KEY] = ratingOperatorKey.first()
                }
            }
        }

        val ratingName = mRatingSpinner.selectedItem.toString()
        parameters[Parameters.RATING_KEY] = ratingName

        val localAuthorityName = mAuthoritySpinner.selectedItem.toString()
        val localAuthorityId = mDatabase.authorityDao().getIdByName(localAuthorityName)
        if (localAuthorityId.size == 1) {
            parameters[Parameters.LOCAL_AUTHORITY_ID] = localAuthorityId.first().toString()
        }

        val sortOptionName = mSortBySpinner.selectedItem.toString()
        val sortOptionKey = mDatabase.sortOptionDao().getKeyByName(sortOptionName)
        if (sortOptionKey.size == 1) {
            parameters[Parameters.SORT_OPTION_KEY] = sortOptionKey.first()
        }

        return parameters
    }
}