package com.example.cleanfood.search

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.cleanfood.EstablishmentAdapter
import com.example.cleanfood.R
import com.example.cleanfood.storage.CleanFoodDatabase
import com.example.cleanfood.storage.Establishment
import com.example.cleanfood.util.Constants
import com.example.cleanfood.util.HttpRequest
import com.example.cleanfood.util.Parameters
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException
import java.util.*

class SearchFragment : Fragment(),
    Callback, FilterDialogFragment.FilterDialogListener {

    companion object {
        private val TAG = SearchFragment::class.java.simpleName

        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    private var mExhaustedResults = false
    private var mPageNumber = 1
    private var mLocation: Location? = null
    private var mParameters: MutableMap<String, String>? = null
    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private var mSearchType: SearchType = SearchType.BY_NAME

    private val mClient = HttpRequest.getClient()
    private val mHandler = Handler()

    private lateinit var mDatabase: CleanFoodDatabase
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mEstablishmentAdapter: EstablishmentAdapter
    private lateinit var mSearchView: SearchView
    private lateinit var mWelcomeLinearLayout: LinearLayout
    private lateinit var mSearchProgressBar: ProgressBar
    private lateinit var mNoResultsLinearLayout: LinearLayout

    enum class SearchType {
        BY_NAME, BY_ADDRESS
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        context?.let {
            mDatabase = CleanFoodDatabase.getDatabase(it)
            if (ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(it)
                mFusedLocationClient.lastLocation.addOnSuccessListener { location ->
                    mLocation = location
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.search_fragment, container, false)

        val filterImageButton = rootView.findViewById(R.id.filter_image_button) as ImageButton
        filterImageButton.setOnClickListener {
            showFilterDialog()
        }

        val titleTextView = rootView.findViewById(R.id.top_search_title_text_view) as TextView
        val hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        when (hour) {
            in 7..10 -> titleTextView.text = resources.getText(R.string.title_breakfast)
            in 11..15 -> titleTextView.text = resources.getText(R.string.title_lunch)
            in 16..22 -> titleTextView.text = resources.getText(R.string.title_dinner)
            else -> titleTextView.text = resources.getText(R.string.title_other)
        }

        mWelcomeLinearLayout = rootView.findViewById(R.id.welcome_linear_layout) as LinearLayout
        mSearchProgressBar = rootView.findViewById(R.id.search_progress_bar) as ProgressBar
        mNoResultsLinearLayout = rootView.findViewById(R.id.no_results_linear_layout) as LinearLayout

        context?.let {

            mRecyclerView = rootView.findViewById(R.id.search_results_recycler_view) as RecyclerView
            mEstablishmentAdapter = EstablishmentAdapter(it)
            val layoutManager = LinearLayoutManager(it)

            mRecyclerView.adapter = mEstablishmentAdapter
            mRecyclerView.layoutManager = layoutManager

            val dividerItemDecoration = DividerItemDecoration(mRecyclerView.context, layoutManager.orientation)
            mRecyclerView.addItemDecoration(dividerItemDecoration)
            mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    when (newState) {
                        RecyclerView.SCROLL_STATE_DRAGGING -> {

                            val innerLayoutManager = recyclerView.layoutManager as LinearLayoutManager

                            val visibleItemCount = innerLayoutManager.childCount
                            val totalItemCount = innerLayoutManager.itemCount
                            val pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition()

                            if (visibleItemCount + pastVisibleItems >= totalItemCount && !mExhaustedResults) {
                                search(resetPageNumber = false)
                            }
                        }
                    }
                }
            })
        }

        mSearchView = rootView.findViewById<View>(R.id.establishment_search_view) as SearchView

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    addSearchViewParameters()
                    search()
                }
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return false
            }
        })

        mSearchView.setOnLongClickListener {

            when (mSearchType) {
                SearchType.BY_NAME -> {
                    mSearchType = SearchType.BY_ADDRESS
                    mSearchView.queryHint = "Search by address"
                }
                SearchType.BY_ADDRESS -> {
                    mSearchType = SearchType.BY_NAME
                    mSearchView.queryHint = "Search by name"
                }
            }
            true
        }

        mHandler.post(FilterCollector(this))

        return rootView
    }

    override fun onResume() {
        super.onResume()
        val favorites = mDatabase.establishmentDao().getFavoriteIds().toHashSet()

        mEstablishmentAdapter.mEstablishments.forEach {
            it.isFavorite = favorites.contains(it.id)
        }
        mEstablishmentAdapter.notifyDataSetChanged()
    }

    private fun showFilterDialog() {
        val dialog = FilterDialogFragment.newInstance(this)
        dialog.show(fragmentManager, "FilterDialogFragment")
    }

    private inner class DataUpdater(private val data: MutableList<Establishment>, private val shouldAppend: Boolean) :
        Runnable {

        override fun run() {
            mSearchProgressBar.visibility = View.GONE

            if (shouldAppend) {
                mEstablishmentAdapter.mEstablishments.addAll(data)
                Log.i(TAG, "Appended more results to the recycler view data set")
            } else {
                if (data.isEmpty()) {
                    mRecyclerView.visibility = View.GONE
                    mNoResultsLinearLayout.visibility = View.VISIBLE
                    return
                }
                mEstablishmentAdapter.mEstablishments = data
                Log.i(TAG, "Updated the recycler view data set")
            }
            mEstablishmentAdapter.notifyDataSetChanged()
            mRecyclerView.visibility = View.VISIBLE
        }
    }

    override fun onFailure(call: Call, e: IOException) {
        Log.e(TAG, "Error occurred while making a request: Exception [$e]")
    }

    override fun onResponse(call: Call, response: Response) {
        if (response.code() != 200) {
            Log.e(TAG, "HTTP request returned with status code ${response.code()}")
            return
        }

        val data = JSONObject(response.body()?.string())

        if (data.has("authorities")) {
            val authorities = HttpRequest.extractAuthorities(data)
            mDatabase.authorityDao().replaceAll(authorities)
            Log.i(TAG, "Cached the authorities")
        }

        if (data.has("businessTypes")) {
            val businessTypes = HttpRequest.extractBusinessTypes(data)
            mDatabase.businessTypeDao().replaceAll(businessTypes)
            Log.i(TAG, "Cached the business types")
        }

        if (data.has("ratings")) {
            val ratings = HttpRequest.extractRatings(data)
            mDatabase.ratingDao().replaceAll(ratings)
            Log.i(TAG, "Cached the ratings")
        }

        if (data.has("ratingOperator")) {
            val ratingOperators = HttpRequest.extractRatingOperators(data)
            mDatabase.ratingOperatorDao().replaceAll(ratingOperators)
            Log.i(TAG, "Cached the rating operators")
        }

        if (data.has("regions")) {
            val regions = HttpRequest.extractRegions(data)
            mDatabase.regionDao().replaceAll(regions)
            Log.i(TAG, "Cached the regions")
        }

        if (data.has("schemeTypes")) {
            val schemeTypes = HttpRequest.extractSchemeTypes(data)
            mDatabase.schemeTypeDao().replaceAll(schemeTypes)
            Log.i(TAG, "Cached the schemeTypes")
        }

        if (data.has("sortOptions")) {
            val sortOptions = HttpRequest.extractSortOptions(data)
            mDatabase.sortOptionDao().replaceAll(sortOptions)
            Log.i(TAG, "Cached the sortOptions")
        }

        if (data.has("establishments")) {
            val establishments = HttpRequest.extractEstablishments(data)

            val shouldAppend = data.getJSONObject("meta").getInt("pageNumber") != 1

            if (establishments.isEmpty() && shouldAppend) {
                mExhaustedResults = true
                mHandler.post {
                    mSearchProgressBar.visibility = View.GONE
                    Toast.makeText(context, "No other results found", Toast.LENGTH_LONG).show()
                }
                Log.i(TAG, "Exhausted results for the search")
                return
            }

            val favorites = mDatabase.establishmentDao().getFavoriteIds().toHashSet()

            establishments.forEach {
                if (favorites.contains(it.id)) {
                    it.isFavorite = true
                }
            }

            mHandler.post(DataUpdater(establishments, shouldAppend))
        }
    }

    override fun onDialogPositiveClick(parameters: MutableMap<String, String>) {
        Log.i(TAG, "Clicked on search inside the dialog")
        mParameters = parameters
        addSearchViewParameters()
        search()
    }

    override fun onDialogNeutralClick(parameters: MutableMap<String, String>) {
        Log.i(TAG, "Clicked on apply inside the dialog")
        mParameters = parameters
    }

    override fun onDialogNegativeClick() {
        Log.i(TAG, "Clicked on clear inside the dialog")
        mParameters = null
    }

    private fun addSearchViewParameters() {
        if (mParameters == null) {
            mParameters = HashMap()
        }

        val query = mSearchView.query.toString()
        if (query != "") {
            when (mSearchType) {
                SearchFragment.SearchType.BY_ADDRESS -> {
                    mParameters?.remove(Parameters.NAME)
                    mParameters?.put(Parameters.ADDRESS, query)
                }
                SearchFragment.SearchType.BY_NAME -> {
                    mParameters?.remove(Parameters.ADDRESS)
                    mParameters?.put(Parameters.NAME, query)
                }
            }
        }
    }

    private fun search(resetPageNumber: Boolean = true) {
        mWelcomeLinearLayout.visibility = View.GONE
        mNoResultsLinearLayout.visibility = View.GONE
        mSearchProgressBar.visibility = View.VISIBLE

        if (resetPageNumber) {
            mRecyclerView.visibility = View.GONE
            mPageNumber = 1
            mExhaustedResults = false
        } else {
            mPageNumber++
        }

        mParameters?.let { parameters ->

            mLocation?.let {
                parameters[Parameters.LONGITUDE] = it.longitude.toString()
                parameters[Parameters.LATITUDE] = it.latitude.toString()
            }

            parameters[Parameters.PAGE_SIZE] = Parameters.PAGE_SIZE_VALUE
            parameters[Parameters.PAGE_NUMBER] = mPageNumber.toString()

            if (!parameters.containsKey(Parameters.MAX_DISTANCE_LIMIT)) {
                parameters[Parameters.MAX_DISTANCE_LIMIT] = "5"

            } else if (parameters[Parameters.MAX_DISTANCE_LIMIT] == "unlimited") {
                parameters.remove(Parameters.LONGITUDE)
                parameters.remove(Parameters.LATITUDE)
                parameters.remove(Parameters.MAX_DISTANCE_LIMIT)
            }

            if (parameters[Parameters.SORT_OPTION_KEY] == "Distance" || !parameters.containsKey(Parameters.SORT_OPTION_KEY)) {
                parameters[Parameters.SORT_OPTION_KEY] = "distance"
            }

            Log.i(TAG, "Performing search with the parameters $parameters")
            mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.ESTABLISHMENTS, parameters)).enqueue(this)
            mSearchView.setQuery("", false)
        }
    }

    private inner class FilterCollector(private val callback: Callback) : Runnable {

        override fun run() {
            if (mDatabase.authorityDao().getCount() == 0) {
                mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.AUTHORITIES, hashMapOf()))
                    .enqueue(callback)
            }

            if (mDatabase.businessTypeDao().getCount() == 0) {
                mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.BUSINESS_TYPES, hashMapOf()))
                    .enqueue(callback)
            }

            if (mDatabase.ratingDao().getCount() == 0) {
                mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.RATINGS, hashMapOf())).enqueue(callback)
            }


            if (mDatabase.ratingOperatorDao().getCount() == 0) {
                mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.RATING_OPERATORS, hashMapOf()))
                    .enqueue(callback)
            }

            if (mDatabase.regionDao().getCount() == 0) {
                mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.REGIONS, hashMapOf())).enqueue(callback)
            }

            if (mDatabase.schemeTypeDao().getCount() == 0) {
                mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.SCHEME_TYPES, hashMapOf()))
                    .enqueue(callback)
            }

            if (mDatabase.sortOptionDao().getCount() == 0) {
                mClient.newCall(HttpRequest.buildRequest(HttpRequest.Type.SORT_OPTION, hashMapOf()))
                    .enqueue(callback)
            }

            if (Constants.TEST_CACHE) {
                Log.i(TAG, mDatabase.authorityDao().getAll().toString())
                Log.i(TAG, mDatabase.businessTypeDao().getAll().toString())
                Log.i(TAG, mDatabase.ratingDao().getAll().toString())
                Log.i(TAG, mDatabase.ratingOperatorDao().getAll().toString())
                Log.i(TAG, mDatabase.regionDao().getAll().toString())
                Log.i(TAG, mDatabase.schemeTypeDao().getAll().toString())
                Log.i(TAG, mDatabase.sortOptionDao().getAll().toString())
            }
        }
    }
}
