package com.example.cleanfood.preferences

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.example.cleanfood.MainActivity
import com.example.cleanfood.R

class AlarmReceiver : BroadcastReceiver() {
    companion object {
        private val TAG = AlarmReceiver::class.java.simpleName
        const val REQUEST_CODE = "REQUEST_CODE"
        const val BREAKFAST_REQUEST_CODE = 0
        const val LUNCH_REQUEST_CODE = 1
        const val DINNER_REQUEST_CODE = 2

        private val contentText =
            arrayOf("Remember to grab breakfast!", "Time for lunch!", "Don't forget about dinner!", "Extra")
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let {

            val requestCode = intent?.getIntExtra(REQUEST_CODE, 0) ?: 0

            val notificationManager = it.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notificationIntent = Intent(it, MainActivity::class.java)

            notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel =
                    NotificationChannel("default", "Reminder notification", NotificationManager.IMPORTANCE_DEFAULT)
                channel.description = "Reminder notification"
                notificationManager.createNotificationChannel(channel)
            }

            val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0)

            val builder = NotificationCompat.Builder(it, "default")
            builder.setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("Meal reminder")
                .setContentText(contentText[requestCode])
                .setContentIntent(pendingIntent)


            notificationManager.notify(1, builder.build())
            Log.i(TAG, "Showed notification")

        }
    }
}