package com.example.cleanfood.preferences

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.TextView
import com.example.cleanfood.R
import java.util.*

class PreferencesFragment : Fragment() {

    companion object {

        private val TAG = PreferencesFragment::class.java.simpleName

        fun newInstance(): PreferencesFragment {
            return PreferencesFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(R.layout.preferences_fragment, container, false)

        val feedbackTextView = rootView.findViewById(R.id.pref_send_feedback_text_view) as TextView
        feedbackTextView.setOnClickListener {

            val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("mailto:")
                putExtra(Intent.EXTRA_EMAIL, arrayOf("robert@chiper.dev"))
                putExtra(Intent.EXTRA_SUBJECT, "CleanFood feedback")
            }
            activity?.let {
                if (emailIntent.resolveActivity(it.packageManager) != null) {
                    startActivity(emailIntent)
                }
            }
        }

        val breakfastSwitch = rootView.findViewById(R.id.pref_breakfast_switch) as Switch
        breakfastSwitch.setOnCheckedChangeListener(switchCallback)

        val lunchSwitch = rootView.findViewById(R.id.pref_lunch_switch) as Switch
        lunchSwitch.setOnCheckedChangeListener(switchCallback)

        val dinnerSwitch = rootView.findViewById(R.id.pref_dinner_switch) as Switch
        dinnerSwitch.setOnCheckedChangeListener(switchCallback)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        sharedPref?.let {
            breakfastSwitch.isChecked = it.getBoolean(getString(R.string.breakfast_notification_key), false)
            lunchSwitch.isChecked = it.getBoolean(getString(R.string.lunch_notification_key), false)
            dinnerSwitch.isChecked = it.getBoolean(getString(R.string.dinner_notification_key), false)
        }

        return rootView
    }

    private fun disableNotification(code: Int) {

        activity?.let {
            val alarmIntent = Intent(it, AlarmReceiver::class.java)
            val alarmManager = it.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            val pendingIntent = PendingIntent.getBroadcast(it, code, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
            pendingIntent?.let { intent ->
                alarmManager.cancel(intent)
                Log.i(TAG, "Disabled repeating alarm for code $code")
            }

            val sharedPref = it.getPreferences(Context.MODE_PRIVATE) ?: return
            when (code) {
                AlarmReceiver.BREAKFAST_REQUEST_CODE -> {
                    with(sharedPref.edit()) {
                        putBoolean(getString(R.string.breakfast_notification_key), false)
                        apply()
                    }

                }
                AlarmReceiver.LUNCH_REQUEST_CODE -> {
                    with(sharedPref.edit()) {
                        putBoolean(getString(R.string.lunch_notification_key), false)
                        apply()
                    }
                }

                AlarmReceiver.DINNER_REQUEST_CODE -> {
                    with(sharedPref.edit()) {
                        putBoolean(getString(R.string.dinner_notification_key), false)
                        apply()
                    }
                }
                else -> return
            }

        }
    }

    private var switchCallback: CompoundButton.OnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            var code = 0

            when (buttonView.id) {
                R.id.pref_breakfast_switch -> code = AlarmReceiver.BREAKFAST_REQUEST_CODE
                R.id.pref_lunch_switch -> code = AlarmReceiver.LUNCH_REQUEST_CODE
                R.id.pref_dinner_switch -> code = AlarmReceiver.DINNER_REQUEST_CODE
            }

            if (isChecked) {
                enableNotification(code)
            } else {
                disableNotification(code)
            }

        }

    private fun enableNotification(code: Int) {
        activity?.let {
            val sharedPref = it.getPreferences(Context.MODE_PRIVATE) ?: return
            val calendar: Calendar

            when (code) {
                AlarmReceiver.BREAKFAST_REQUEST_CODE -> {
                    with(sharedPref.edit()) {
                        putBoolean(getString(R.string.breakfast_notification_key), true)
                        apply()
                    }
                    calendar = generateCalendar(9)

                }
                AlarmReceiver.LUNCH_REQUEST_CODE -> {
                    with(sharedPref.edit()) {
                        putBoolean(getString(R.string.lunch_notification_key), true)
                        apply()
                    }
                    calendar = generateCalendar(12)
                }

                AlarmReceiver.DINNER_REQUEST_CODE -> {
                    with(sharedPref.edit()) {
                        putBoolean(getString(R.string.dinner_notification_key), true)
                        apply()
                    }
                    calendar = generateCalendar(18, 30)
                }
                else -> return
            }

            if (calendar.before(Calendar.getInstance())) {
                calendar.add(Calendar.DATE, 1)
            }


            val alarmIntent = Intent(it, AlarmReceiver::class.java)
            alarmIntent.putExtra(AlarmReceiver.REQUEST_CODE, code)

            val pendingIntent =
                PendingIntent.getBroadcast(it, code, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val alarmManager = it.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntent
            )
            Log.i(TAG, "Created repeating alarm for code $code")

        }
    }

    private fun generateCalendar(hour: Int, minute: Int = 0): Calendar {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute)
        calendar.set(Calendar.SECOND, 0)
        if (calendar.before(Calendar.getInstance())) {
            calendar.add(Calendar.DATE, 1)
        }
        return calendar
    }
}