package com.example.cleanfood.favorites

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.cleanfood.EstablishmentAdapter
import com.example.cleanfood.R
import com.example.cleanfood.storage.CleanFoodDatabase
import com.example.cleanfood.storage.Establishment

class FavoritesFragment : Fragment() {

    private val mHandler = Handler()

    private enum class SortType {
        BEST, WORST, AZ, ZA
    }

    private var mPrimaryColor: Int = 0

    private lateinit var mTitleTextView: TextView
    private lateinit var mSortType: SortType
    private lateinit var mBestButton: Button
    private lateinit var mWorstButton: Button
    private lateinit var mAZButton: Button
    private lateinit var mZAButton: Button
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mEstablishmentAdapter: EstablishmentAdapter
    private lateinit var mDatabase: CleanFoodDatabase

    companion object {

        fun newInstance(): FavoritesFragment {
            return FavoritesFragment()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            mPrimaryColor = ContextCompat.getColor(it, R.color.colorPrimary)
            mDatabase = CleanFoodDatabase.getDatabase(it)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.favorites_fragment, container, false)

        context?.let {
            mRecyclerView = rootView.findViewById(R.id.fav_recycler_view) as RecyclerView
            mEstablishmentAdapter = EstablishmentAdapter(it)
            val layoutManager = LinearLayoutManager(activity)

            mRecyclerView.adapter = mEstablishmentAdapter
            mRecyclerView.layoutManager = layoutManager

            val dividerItemDecoration = DividerItemDecoration(mRecyclerView.context, layoutManager.orientation)
            mRecyclerView.addItemDecoration(dividerItemDecoration)
        }

        mTitleTextView = rootView.findViewById(R.id.fav_top_title)
        mBestButton = rootView.findViewById(R.id.fav_best_button) as Button
        mWorstButton = rootView.findViewById(R.id.fav_worst_button) as Button
        mAZButton = rootView.findViewById(R.id.fav_az_button) as Button
        mZAButton = rootView.findViewById(R.id.fav_za_button) as Button

        mSortType = SortType.BEST
        mBestButton.setTextColor(Color.WHITE)
        mBestButton.setBackgroundResource(R.drawable.sort_button_filled)

        mBestButton.setOnClickListener {
            if (mSortType != SortType.BEST) {
                when (mSortType) {
                    SortType.WORST -> {
                        mWorstButton.setTextColor(mPrimaryColor)
                        mWorstButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.AZ -> {
                        mAZButton.setTextColor(mPrimaryColor)
                        mAZButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.ZA -> {
                        mZAButton.setTextColor(mPrimaryColor)
                        mZAButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    else -> {
                    }
                }
                mBestButton.setTextColor(Color.WHITE)
                mBestButton.setBackgroundResource(R.drawable.sort_button_filled)
                mSortType = SortType.BEST
                mEstablishmentAdapter.mEstablishments.sortWith(BestComparator())
                mEstablishmentAdapter.notifyDataSetChanged()
            }
        }

        mWorstButton.setOnClickListener {
            if (mSortType != SortType.WORST) {
                when (mSortType) {
                    SortType.BEST -> {
                        mBestButton.setTextColor(mPrimaryColor)
                        mBestButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.AZ -> {
                        mAZButton.setTextColor(mPrimaryColor)
                        mAZButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.ZA -> {
                        mZAButton.setTextColor(mPrimaryColor)
                        mZAButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    else -> {
                    }
                }
                mWorstButton.setTextColor(Color.WHITE)
                mWorstButton.setBackgroundResource(R.drawable.sort_button_filled)
                mSortType = SortType.WORST
                mEstablishmentAdapter.mEstablishments.sortWith(WorstComparator())
                mEstablishmentAdapter.notifyDataSetChanged()
            }
        }

        mAZButton.setOnClickListener {
            if (mSortType != SortType.AZ) {
                when (mSortType) {
                    SortType.BEST -> {
                        mBestButton.setTextColor(mPrimaryColor)
                        mBestButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.WORST -> {
                        mWorstButton.setTextColor(mPrimaryColor)
                        mWorstButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.ZA -> {
                        mZAButton.setTextColor(mPrimaryColor)
                        mZAButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    else -> {
                    }
                }
                mAZButton.setTextColor(Color.WHITE)
                mAZButton.setBackgroundResource(R.drawable.sort_button_filled)
                mSortType = SortType.AZ
                mEstablishmentAdapter.mEstablishments.sortWith(AZComparator())
                mEstablishmentAdapter.notifyDataSetChanged()
            }
        }

        mZAButton.setOnClickListener {
            if (mSortType != SortType.ZA) {
                when (mSortType) {
                    SortType.BEST -> {
                        mBestButton.setTextColor(mPrimaryColor)
                        mBestButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.WORST -> {
                        mWorstButton.setTextColor(mPrimaryColor)
                        mWorstButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    SortType.AZ -> {
                        mAZButton.setTextColor(mPrimaryColor)
                        mAZButton.setBackgroundResource(R.drawable.sort_button_border)
                    }
                    else -> {
                    }
                }
                mZAButton.setTextColor(Color.WHITE)
                mZAButton.setBackgroundResource(R.drawable.sort_button_filled)
                mSortType = SortType.ZA
                mEstablishmentAdapter.mEstablishments.sortWith(ZAComparator())
                mEstablishmentAdapter.notifyDataSetChanged()
            }
        }

        mHandler.post {
            updateFavorites()
        }

        return rootView
    }

    override fun onResume() {
        super.onResume()
        updateFavorites()
    }

    private fun updateFavorites() {
        val favorites = mDatabase.establishmentDao().getAllFavorites()
        favorites.forEach { it.distance = -1.0 }
        when (mSortType) {
            SortType.BEST -> favorites.sortWith(BestComparator())
            SortType.WORST -> favorites.sortWith(WorstComparator())
            SortType.AZ -> favorites.sortWith(AZComparator())
            SortType.ZA -> favorites.sortWith(ZAComparator())
        }
        mTitleTextView.text = if (favorites.size == 1) {
            "You have 1 favorite…"
        } else {
            "You have ${favorites.size} favorites…"
        }
        mEstablishmentAdapter.mEstablishments = favorites
        mEstablishmentAdapter.notifyDataSetChanged()

        context?.let {
            when (favorites.size) {
                7 -> Toast.makeText(it, "CR7 favorites, SIIIUUU!", Toast.LENGTH_LONG).show()
                99 -> Toast.makeText(it, "Cool cool cool, noice, toit!", Toast.LENGTH_LONG).show()
            }
        }

    }

    private inner class AZComparator : Comparator<Establishment> {
        override fun compare(o1: Establishment, o2: Establishment): Int {
            return o1.name.compareTo(o2.name)
        }
    }

    private inner class ZAComparator : Comparator<Establishment> {
        override fun compare(o1: Establishment, o2: Establishment): Int {
            return o2.name.compareTo(o1.name)
        }
    }

    private inner class BestComparator : Comparator<Establishment> {
        override fun compare(o1: Establishment, o2: Establishment): Int {
            return o2.ratingValue.compareTo(o1.ratingValue)
        }
    }

    private inner class WorstComparator : Comparator<Establishment> {
        override fun compare(o1: Establishment, o2: Establishment): Int {
            return o1.ratingValue.compareTo(o2.ratingValue)
        }
    }

}