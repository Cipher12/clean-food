package com.example.cleanfood.util

object Endpoints {
    const val GET_ESTABLISHMENTS = "http://api.ratings.food.gov.uk/Establishments"
    const val GET_BUSINESS_TYPES = "http://api.ratings.food.gov.uk/BusinessTypes"
    const val GET_AUTHORITIES = "http://api.ratings.food.gov.uk/Authorities"
    const val GET_REGIONS = "http://api.ratings.food.gov.uk/Regions"
    const val GET_SCHEME_TYPES = "http://api.ratings.food.gov.uk/SchemeTypes"
    const val GET_SORT_OPTIONS = "http://api.ratings.food.gov.uk/SortOptions"
    const val GET_SCORE_DESCRIPTORS = "http://api.ratings.food.gov.uk/ScoreDescriptors"
    const val GET_RATINGS = "http://api.ratings.food.gov.uk/Ratings"
    const val GET_RATING_OPERATORS = "http://api.ratings.food.gov.uk/RatingOperators"

}