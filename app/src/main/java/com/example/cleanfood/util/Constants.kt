package com.example.cleanfood.util

object Constants {
    const val TEST_CACHE = false
    const val TEST_DATABASE = false
    const val TEST_CLIENT = false

    const val ESTABLISHMENT_ID = "establishmentId"
}