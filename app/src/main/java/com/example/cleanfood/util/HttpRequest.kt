package com.example.cleanfood.util

import android.os.Build
import android.util.Log
import com.example.cleanfood.storage.*
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class HttpRequest {

    companion object {

        private val TAG = HttpRequest::class.java.simpleName

        private var clientInstance: OkHttpClient? = null

        private val dateFormatter = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
        } else {
            null
        }

        fun getClient(): OkHttpClient {
            if (clientInstance == null) {
                clientInstance = OkHttpClient()

            }
            return clientInstance!!
        }

        private fun buildUrl(type: Type, parameters: Map<String, String>): String {
            val builder = StringBuilder()

            when (type) {
                Type.ESTABLISHMENTS -> builder.append(Endpoints.GET_ESTABLISHMENTS)
                Type.BUSINESS_TYPES -> builder.append(Endpoints.GET_BUSINESS_TYPES)
                Type.AUTHORITIES -> builder.append(Endpoints.GET_AUTHORITIES)
                Type.REGIONS -> builder.append(Endpoints.GET_REGIONS)
                Type.SCHEME_TYPES -> builder.append(Endpoints.GET_SCHEME_TYPES)
                Type.SORT_OPTION -> builder.append(Endpoints.GET_SORT_OPTIONS)
                Type.SCORE_DESCRIPTORS -> builder.append(Endpoints.GET_SCORE_DESCRIPTORS)
                Type.RATINGS -> builder.append(Endpoints.GET_RATINGS)
                Type.RATING_OPERATORS -> builder.append(Endpoints.GET_RATING_OPERATORS)
            }

            if (parameters.isNotEmpty()) {
                builder.append('?')
                parameters.forEach { (key, value) ->
                    builder.append(key)
                    builder.append('=')
                    builder.append(value)
                    builder.append('&')

                }
                builder.deleteCharAt(builder.length - 1)

            }


            val url = builder.toString()
            if (!Constants.TEST_CLIENT) {
                Log.i(TAG, "Generated url is [$url]")
            }
            return url
        }

        fun buildRequest(type: Type, parameters: Map<String, String>): Request {
            return Request.Builder()
                .url(buildUrl(type, parameters))
                .addHeader("x-api-version", "2")
                .build()
        }

        fun extractEstablishments(jsonData: JSONObject): MutableList<Establishment> {
            val result = ArrayList<Establishment>()

            val establishments = jsonData.getJSONArray("establishments")

            for (i in 0..(establishments.length() - 1)) {
                val e = establishments.get(i) as JSONObject
                val id = e.getInt("FHRSID")
                val addressBuilder = StringBuilder()
                for (j in 1..4) {
                    val addressLine = e.optString("AddressLine$j", "")
                    if (addressLine.isNotEmpty()) {
                        addressBuilder.append(addressLine)
                        addressBuilder.append(", ")
                    }
                }
                if (addressBuilder.length > 1) {
                    addressBuilder.delete(addressBuilder.length - 2, addressBuilder.length)
                }

                val scores = e.getJSONObject("scores")
                val geocode = e.getJSONObject("geocode")
                val ratingDate = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val ratingDateString = e.optString("RatingDate", "")
                    if (ratingDateString != "") {
                        LocalDateTime.parse(ratingDateString, dateFormatter).toEpochSecond(ZoneOffset.UTC)
                    } else {
                        0L
                    }
                } else {
                    0L
                }

                result.add(
                    Establishment(
                        id,
                        e.optString("BusinessName", ""),
                        e.optInt("BusinessTypeID", -1),
                        e.optString("BusinessType", ""),
                        addressBuilder.toString(),
                        e.optString("PostCode", ""),
                        e.optString("Phone", ""),
                        e.optString("RatingValue", ""),
                        e.optString("RatingKey", ""),
                        ratingDate,
                        scores.optInt("Hygiene", 0),
                        scores.optInt("Structural", 0),
                        scores.optInt("ConfidenceInManagement", 0),
                        e.optString("SchemeType", ""),
                        e.optInt("LocalAuthorityCode", 0),
                        geocode.optDouble("longitude", 0.0),
                        geocode.optDouble("latitude", 0.0),
                        e.optDouble("Distance", -1.0),
                        false
                    )
                )
            }
            return result
        }

        fun extractAuthorities(jsonData: JSONObject): Collection<Authority> {
            val result = ArrayList<Authority>()

            val authorities = jsonData.getJSONArray("authorities")

            for (i in 0 until authorities.length()) {
                val a = authorities[i] as JSONObject

                result.add(
                    Authority(
                        id = a.getInt("LocalAuthorityId"),
                        idCode = a.optString("LocalAuthorityIdCode", ""),
                        name = a.optString("Name", ""),
                        url = a.optString("Url", ""),
                        email = a.optString("Email", ""),
                        regionName = a.optString("RegionName", ""),
                        establishmentCount = a.optInt("EstablishmentCount", 0),
                        schemeType = a.optInt("SchemeType", 0)
                    )
                )
            }

            return result
        }

        fun extractBusinessTypes(jsonData: JSONObject): Collection<BusinessType> {
            val result = ArrayList<BusinessType>()

            val businessTypes = jsonData.getJSONArray("businessTypes")

            for (i in 0 until businessTypes.length()) {
                val b = businessTypes[i] as JSONObject

                result.add(
                    BusinessType(
                        id = b.getInt("BusinessTypeId"),
                        name = b.optString("BusinessTypeName", "")
                    )
                )
            }
            return result
        }

        fun extractRatings(jsonData: JSONObject): Collection<Rating> {
            val result = ArrayList<Rating>()

            val ratings = jsonData.getJSONArray("ratings")

            for (i in 0 until ratings.length()) {
                val r = ratings[i] as JSONObject

                result.add(
                    Rating(
                        id = r.getInt("ratingId"),
                        name = r.optString("ratingName", ""),
                        key = r.optString("ratingKey", ""),
                        keyName = r.optString("ratingKeyName", ""),
                        schemeTypeId = r.optInt("schemeTypeId", 0)
                    )
                )
            }

            return result
        }

        fun extractRegions(jsonData: JSONObject): Collection<Region> {
            val result = ArrayList<Region>()

            val regions = jsonData.getJSONArray("regions")

            for (i in 0 until regions.length()) {
                val r = regions[i] as JSONObject

                result.add(
                    Region(
                        id = r.getInt("id"),
                        name = r.optString("name", ""),
                        nameKey = r.optString("nameKey", ""),
                        code = r.optString("code", "")
                    )
                )
            }

            return result
        }

        fun extractSchemeTypes(jsonData: JSONObject): Collection<SchemeType> {
            val result = ArrayList<SchemeType>()

            val schemeTypes = jsonData.getJSONArray("schemeTypes")

            for (i in 0 until schemeTypes.length()) {
                val s = schemeTypes[i] as JSONObject

                result.add(
                    SchemeType(
                        id = s.getInt("schemeTypeid"),
                        name = s.optString("schemeTypeName", ""),
                        key = s.optString("schemeTypeKey", "")
                    )
                )
            }

            return result
        }

        fun extractSortOptions(jsonData: JSONObject): Collection<SortOption> {
            val result = ArrayList<SortOption>()

            val sortOptions = jsonData.getJSONArray("sortOptions")

            for (i in 0 until sortOptions.length()) {
                val s = sortOptions[i] as JSONObject

                result.add(
                    SortOption(
                        id = s.getInt("sortOptionId"),
                        name = s.optString("sortOptionName", ""),
                        key = s.optString("sortOptionKey", "")
                    )
                )
            }

            return result
        }

        fun extractRatingOperators(jsonData: JSONObject): Collection<RatingOperator> {
            val result = ArrayList<RatingOperator>()

            val ratingOperators = jsonData.getJSONArray("ratingOperator")

            for (i in 0 until ratingOperators.length()) {
                val s = ratingOperators[i] as JSONObject

                result.add(
                    RatingOperator(
                        id = s.getInt("ratingOperatorId"),
                        name = s.optString("ratingOperatorName", ""),
                        key = s.optString("ratingOperatorKey", "")
                    )
                )
            }

            return result
        }
    }

    enum class Type {
        ESTABLISHMENTS,
        BUSINESS_TYPES,
        AUTHORITIES,
        REGIONS,
        SCHEME_TYPES,
        SORT_OPTION,
        SCORE_DESCRIPTORS,
        RATINGS,
        RATING_OPERATORS
    }
}