package com.example.cleanfood.util

object Parameters {
    const val NAME = "name"
    const val ADDRESS = "address"
    const val LONGITUDE = "longitude"
    const val LATITUDE = "latitude"
    const val MAX_DISTANCE_LIMIT = "maxDistanceLimit"
    const val BUSINESS_TYPE_ID = "businessTypeId"
    const val SCHEME_TYPE_KEY = "schemeTypeKey"
    const val RATING_KEY = "ratingKey"
    const val RATING_OPERATOR_KEY = "ratingOperatorKey"
    const val LOCAL_AUTHORITY_ID = "localAuthorityId"
    const val COUNTRY_ID = "countryId"
    const val SORT_OPTION_KEY = "sortOptionKey"
    const val PAGE_NUMBER = "pageNumber"
    const val PAGE_SIZE = "pageSize"
    const val PAGE_SIZE_VALUE = "20"
    const val DISTANCE_UNIT = "distanceUnit"
    const val REGION = "region"
}