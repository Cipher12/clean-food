package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface AuthorityDao {

    @Query("SELECT * FROM authorities WHERE idCode = :idCode")
    fun getByIdCode(idCode: Int): List<Authority>

    @Query("SELECT * FROM authorities WHERE schemeType = :schemeType")
    fun getAllWithSchemeType(schemeType: Int): List<Authority>

    @Query("SELECT name FROM authorities WHERE schemeType = :schemeType ORDER BY name")
    fun getAllNamesBySchemeType(schemeType: Int): List<String>

    @Query("SELECT name FROM authorities WHERE schemeType = :schemeType AND regionName = :region ORDER BY name")
    fun getAllNamesBySchemeTypeAndRegion(schemeType: Int, region: String): List<String>

    @Query("SELECT id FROM authorities WHERE name = :name")
    fun getIdByName(name: String): List<Int>

    @Query("SELECT * FROM authorities")
    fun getAll(): List<Authority>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(authority: Authority)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(authorities: Collection<Authority>)

    @Query("DELETE FROM authorities")
    fun deleteAll()

    @Transaction
    fun replaceAll(authorities: Collection<Authority>) {
        deleteAll()
        insertAll(authorities)
    }

    @Query("SELECT COUNT(*) FROM authorities")
    fun getCount(): Int
}