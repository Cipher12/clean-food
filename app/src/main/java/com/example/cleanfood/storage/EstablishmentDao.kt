package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface EstablishmentDao {

    @Delete
    fun delete(establishment: Establishment)

    @Query("SELECT * FROM establishments WHERE isFavorite = 1")
    fun getAllFavorites(): MutableList<Establishment>

    @Query("SELECT id FROM establishments WHERE isFavorite = 1")
    fun getFavoriteIds(): List<Int>

    @Query("SELECT * FROM establishments")
    fun getAll(): List<Establishment>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(establishment: Establishment)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(establishment: Collection<Establishment>)
}