package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface RatingOperatorDao {

    @Query("SELECT name FROM ratingOperators")
    fun getAllNames(): List<String>

    @Query("SELECT `key` FROM ratingOperators WHERE name = :name")
    fun getKeyByName(name: String): List<String>

    @Query("SELECT * FROM ratingOperators")
    fun getAll(): List<RatingOperator>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(ratingOperators: Collection<RatingOperator>)

    @Query("DELETE FROM ratingOperators")
    fun deleteAll()

    @Transaction
    fun replaceAll(ratingOperators: Collection<RatingOperator>) {
        deleteAll()
        insertAll(ratingOperators)
    }

    @Query("SELECT COUNT(*) FROM ratingOperators")
    fun getCount(): Int
}