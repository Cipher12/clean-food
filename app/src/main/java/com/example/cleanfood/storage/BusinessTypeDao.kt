package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface BusinessTypeDao {
    @Query("SELECT name FROM businessTypes WHERE id >= 0 ORDER BY name")
    fun getAllNames(): List<String>

    @Query("SELECT id FROM businessTypes WHERE name = :name")
    fun getIdByName(name: String): List<Int>

    @Query("SELECT * FROM businessTypes")
    fun getAll(): List<BusinessType>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(businessType: BusinessType)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(businessTypes: Collection<BusinessType>)

    @Query("DELETE FROM businessTypes")
    fun deleteAll()

    @Transaction
    fun replaceAll(businessTypes: Collection<BusinessType>) {
        deleteAll()
        insertAll(businessTypes)
    }

    @Query("SELECT COUNT(*) FROM businessTypes")
    fun getCount(): Int
}