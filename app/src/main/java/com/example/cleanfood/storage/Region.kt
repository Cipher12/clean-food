package com.example.cleanfood.storage

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "regions")
data class Region(
    @PrimaryKey var id: Int,
    @ColumnInfo var name: String,
    @ColumnInfo var nameKey: String,
    @ColumnInfo var code: String
)