package com.example.cleanfood.storage

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "businessTypes")
data class BusinessType(
    @PrimaryKey var id: Int,
    @ColumnInfo var name: String
)