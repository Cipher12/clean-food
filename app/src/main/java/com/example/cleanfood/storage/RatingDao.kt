package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface RatingDao {

    @Query("SELECT name FROM ratings WHERE schemeTypeId = :schemeType ORDER BY id DESC")
    fun getAllNamesBySchemeType(schemeType: Int): List<String>

    @Query("SELECT * FROM ratings")
    fun getAll(): List<Rating>

    @Query("SELECT `key` FROM ratings WHERE name = :name")
    fun getKeyByName(name: String): List<String>

    @Query("SELECT * FROM ratings WHERE schemeTypeId = :schemeTypeId")
    fun getAllWithSchemeTypeId(schemeTypeId: Int): List<Rating>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(ratings: Collection<Rating>)

    @Query("DELETE FROM ratings")
    fun deleteAll()

    @Transaction
    fun replaceAll(ratings: Collection<Rating>) {
        deleteAll()
        insertAll(ratings)
    }

    @Query("SELECT COUNT(*) FROM ratings")
    fun getCount(): Int
}