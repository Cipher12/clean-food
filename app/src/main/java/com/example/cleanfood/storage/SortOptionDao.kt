package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface SortOptionDao {
    @Query("SELECT name FROM sortOptions ORDER BY name")
    fun getAllNames(): List<String>

    @Query("SELECT `key` FROM sortOptions WHERE name = :name")
    fun getKeyByName(name: String): List<String>

    @Query("SELECT * FROM sortOptions")
    fun getAll(): List<SortOption>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(sortOption: SortOption)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(sortOptions: Collection<SortOption>)

    @Query("DELETE FROM sortOptions")
    fun deleteAll()

    @Transaction
    fun replaceAll(sortOptions: Collection<SortOption>) {
        deleteAll()
        insertAll(sortOptions)
    }

    @Query("SELECT COUNT(*) FROM sortOptions")
    fun getCount(): Int
}