package com.example.cleanfood.storage

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.util.Log
import com.example.cleanfood.util.Constants


@Database(
    entities = [Authority::class, BusinessType::class, Establishment::class,
        Rating::class, Region::class, SchemeType::class, SortOption::class,
        RatingOperator::class],
    version = 7,
    exportSchema = false
)
abstract class CleanFoodDatabase : RoomDatabase() {
    abstract fun authorityDao(): AuthorityDao
    abstract fun businessTypeDao(): BusinessTypeDao
    abstract fun establishmentDao(): EstablishmentDao
    abstract fun ratingDao(): RatingDao
    abstract fun ratingOperatorDao(): RatingOperatorDao
    abstract fun regionDao(): RegionDao
    abstract fun schemeTypeDao(): SchemeTypeDao
    abstract fun sortOptionDao(): SortOptionDao

    companion object {
        private val TAG = CleanFoodDatabase::class.java.simpleName

        private const val DATABASE_NAME = "clean_food_db"
        private var dbInstance: CleanFoodDatabase? = null

        fun getDatabase(context: Context): CleanFoodDatabase {
            if (dbInstance == null) {
                dbInstance = if (Constants.TEST_DATABASE) {
                    Room.inMemoryDatabaseBuilder(context, CleanFoodDatabase::class.java)
                        .allowMainThreadQueries()
                        .build()
                } else {
                    Room.databaseBuilder(context, CleanFoodDatabase::class.java, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .build()
                }
            }
            Log.i(TAG, "Returning db instance")
            return dbInstance!!
        }

        fun close() {
            Log.i(TAG, "Closing db")
            dbInstance?.close()
        }
    }

}