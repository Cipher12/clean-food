package com.example.cleanfood.storage

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.NO_ACTION
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "authorities")
data class Authority(
    @PrimaryKey var id: Int,
    @ColumnInfo var idCode: String,
    @ColumnInfo var name: String,
    @ColumnInfo var url: String,
    @ColumnInfo var email: String,
    @ColumnInfo @ForeignKey(
        entity = Region::class,
        parentColumns = ["name"],
        childColumns = ["regionName"],
        onDelete = NO_ACTION
    ) var regionName: String,
    @ColumnInfo var establishmentCount: Int,
    @ColumnInfo @ForeignKey(
        entity = SchemeType::class,
        parentColumns = ["id"],
        childColumns = ["schemeType"],
        onDelete = ForeignKey.NO_ACTION
    ) var schemeType: Int
)

