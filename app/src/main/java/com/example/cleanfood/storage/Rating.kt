package com.example.cleanfood.storage

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "ratings")
data class Rating(
    @PrimaryKey var id: Int,
    @ColumnInfo var name: String,
    @ColumnInfo var key: String,
    @ColumnInfo var keyName: String,
    @ColumnInfo @ForeignKey(
        entity = SchemeType::class,
        parentColumns = ["id"],
        childColumns = ["schemeTypeId"],
        onDelete = ForeignKey.NO_ACTION
    ) var schemeTypeId: Int
)