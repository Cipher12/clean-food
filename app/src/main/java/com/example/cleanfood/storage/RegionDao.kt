package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface RegionDao {
    @Query("SELECT name FROM regions ORDER BY name")
    fun getAllNames(): List<String>

    @Query("SELECT * FROM regions")
    fun getAll(): List<Region>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(regions: Region)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(regions: Collection<Region>)

    @Query("DELETE FROM regions")
    fun deleteAll()

    @Transaction
    fun replaceAll(regions: Collection<Region>) {
        deleteAll()
        insertAll(regions)
    }

    @Query("SELECT COUNT(*) FROM regions")
    fun getCount(): Int
}