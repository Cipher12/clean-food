package com.example.cleanfood.storage

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.NO_ACTION
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "establishments")
data class Establishment(

    @PrimaryKey var id: Int,
    @ColumnInfo var name: String,
    @ColumnInfo @ForeignKey(
        entity = BusinessType::class,
        parentColumns = ["id"],
        childColumns = ["businessTypeId"],
        onDelete = NO_ACTION
    ) var businessTypeId: Int,
    @ColumnInfo var businessTypeName: String,
    @ColumnInfo var address: String,
    @ColumnInfo var postCode: String,
    @ColumnInfo var phone: String,
    @ColumnInfo var ratingValue: String,
    @ColumnInfo @ForeignKey(
        entity = Rating::class,
        parentColumns = ["key"],
        childColumns = ["ratingKey"],
        onDelete = NO_ACTION
    ) var ratingKey: String,
    @ColumnInfo var ratingTimestamp: Long,
    @ColumnInfo var hygieneScore: Int,
    @ColumnInfo var structuralScore: Int,
    @ColumnInfo var confidenceInManagementScore: Int,
    @ColumnInfo @ForeignKey(
        entity = SchemeType::class,
        parentColumns = ["schemeTypeName"],
        childColumns = ["schemeType"],
        onDelete = NO_ACTION
    ) var schemeType: String,
    @ColumnInfo @ForeignKey(
        entity = Authority::class,
        parentColumns = ["idCode"],
        childColumns = ["localAuthorityCode"],
        onDelete = NO_ACTION
    ) var localAuthorityCode: Int,
    @ColumnInfo var longitude: Double,
    @ColumnInfo var latitude: Double,
    @ColumnInfo var distance: Double,
    @ColumnInfo var isFavorite: Boolean
) : Serializable