package com.example.cleanfood.storage

import android.arch.persistence.room.*

@Dao
interface SchemeTypeDao {

    @Query("SELECT name FROM schemeTypes ORDER BY id")
    fun getAllNames(): List<String>

    @Query("SELECT id FROM schemeTypes WHERE name = :name")
    fun getIdByName(name: String): List<Int>

    @Query("SELECT `key` FROM schemeTypes WHERE name = :name")
    fun getKeyByName(name: String): List<String>

    @Query("SELECT * FROM schemeTypes")
    fun getAll(): List<SchemeType>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(schemeType: SchemeType)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(schemeTypes: Collection<SchemeType>)

    @Query("DELETE FROM schemeTypes")
    fun deleteAll()

    @Transaction
    fun replaceAll(schemeType: Collection<SchemeType>) {
        deleteAll()
        insertAll(schemeType)
    }

    @Query("SELECT COUNT(*) FROM schemeTypes")
    fun getCount(): Int
}