package com.example.cleanfood.util

import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Test

class HttpRequestTest {

    private lateinit var client: OkHttpClient

    @Before
    fun setUp() {
        client = HttpRequest.getClient()
    }

    @Test
    fun establishmentsRequest() {
        val request = HttpRequest
            .buildRequest(
                HttpRequest.Type.ESTABLISHMENTS,
                hashMapOf(Parameters.NAME to "Subway", Parameters.PAGE_NUMBER to "1", Parameters.PAGE_SIZE to "25")
            )

        val response = client.newCall(request).execute()

        println(response.body()!!.string())
//        val establishments = HttpRequest.extractEstablishments(response.body()?.string()!!)
//        assert(establishments.size == 25)
    }
}