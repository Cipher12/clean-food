package com.example.cleanfood.storage

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.AfterClass
import org.junit.Assert.assertEquals
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AuthorityTest {

    companion object {

        private lateinit var db: CleanFoodDatabase
        private lateinit var authorityDao: AuthorityDao

        @BeforeClass
        @JvmStatic
        fun setUp() {
            db = CleanFoodDatabase.getDatabase(InstrumentationRegistry.getContext())
            authorityDao = db.authorityDao()

        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            CleanFoodDatabase.close()
        }
    }

    @After
    fun deleteAll() {
        authorityDao.deleteAll()
    }

    @Test
    fun singleAuthorityTest() {

        val authority = Authority(
            1, "760", "Aberdeen City", "http://www.aberdeencity.gov.uk",
            "commercial@aberdeencity.gov.uk", "Scotland", 1802, 2
        )

        authorityDao.insert(authority)

        val all = authorityDao.getAll()
        assertEquals(all.size, 1)
        assertEquals(all[0].id, 1)
        assertEquals(all[0].name, "Aberdeen City")

        val allWithScheme1 = authorityDao.getAllWithSchemeType(1)
        assertEquals(allWithScheme1.size, 0)

        val allWithScheme2 = authorityDao.getAllWithSchemeType(2)
        assertEquals(allWithScheme2.size, 1)
        assertEquals(allWithScheme2[0].id, 1)
        assertEquals(allWithScheme2[0].name, "Aberdeen City")

        val allMinimalWithScheme2 = authorityDao.getAllWithSchemeType(2)
        assertEquals(allMinimalWithScheme2.size, 1)
        assertEquals(allMinimalWithScheme2[0].id, 1)
        assertEquals(allMinimalWithScheme2[0].name, "Aberdeen City")

        val count = authorityDao.getCount()
        assertEquals(count, 1)
    }

    @Test
    fun multipleAuthoritiesTest() {
        val authorities = ArrayList<Authority>()
        for (i in 0 until 392) {
            authorities.add(
                Authority(
                    i, "760", "Aberdeen City", "http://www.aberdeencity.gov.uk",
                    "commercial@aberdeencity.gov.uk", "Scotland", 1802, 2
                )
            )
        }

        authorityDao.insertAll(authorities)

        val all = authorityDao.getAll()
        assertEquals(all.size, 392)
        for (i in 0 until 392) {
            assertEquals(all[i].id, i)
        }

        val count1 = authorityDao.getCount()
        assertEquals(count1, 392)

        authorityDao.replaceAll(authorities)
        val count2 = authorityDao.getCount()
        assertEquals(count2, 392)

        authorityDao.deleteAll()
        val count3 = authorityDao.getCount()
        assertEquals(count3, 0)
    }
}