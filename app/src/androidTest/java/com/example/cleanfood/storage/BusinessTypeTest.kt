package com.example.cleanfood.storage

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.AfterClass
import org.junit.Assert.assertEquals
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BusinessTypeTest {

    companion object {

        private lateinit var db: CleanFoodDatabase
        private lateinit var businessTypeDao: BusinessTypeDao

        @BeforeClass
        @JvmStatic
        fun setUp() {
            db = CleanFoodDatabase.getDatabase(InstrumentationRegistry.getContext())
            businessTypeDao = db.businessTypeDao()
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            CleanFoodDatabase.close()
        }
    }

    @After
    fun deleteAll() {
        businessTypeDao.deleteAll()
    }

    @Test
    @Throws(Exception::class)
    fun singleBusinessTypeTest() {
        val businessType = BusinessType(1, "Restaurant/Cafe/Canteen")
        businessTypeDao.insert(businessType)

        val all = businessTypeDao.getAll()
        assertEquals(all.size, 1)
        assertEquals(all[0].id, 1)
        assertEquals(all[0].name, "Restaurant/Cafe/Canteen")
    }

    @Test
    @Throws(Exception::class)
    fun multipleBusinessTypesTest() {
        val businessTypes = ArrayList<BusinessType>()

        for (i in 0 until 15) {
            businessTypes.add(BusinessType(i, ""))
        }

        businessTypeDao.insertAll(businessTypes)

        val all = businessTypeDao.getAll()
        assertEquals(all.size, 15)

        for (i in 0 until 15) {
            assertEquals(all[i].id, i)
        }
    }
}